<?php

/**
 * @file
 * Example batch implementation for Batch API backport to Drupal 5.
 *
 * Only difference to D6+'s Batch API is that Form API does not invoke any
 * batch sets automatically. Developers need to invoke batch_process() manually
 * in a custom menu callback or form submit handler.
 */

/**
 * Implementation of hook_menu().
 */
function batch_example_menu($may_cache) {
  $items = array();
  if ($may_cache) {
    $items[] = array(
      'path' => 'batch-example',
      'title' => 'Example batch', // Not used here; overridden by $batch['title'].
      'callback' => 'batch_example_page',
      'access' => TRUE,
    );
  }
  return $items;
}

/**
 * Menu callback; Invoke a batch.
 *
 * Note that you do not need a menu callback to invoke a batch. You can do the
 * same in a form submit handler.
 */
function batch_example_page() {
  // Initialize batch (to set title).
  $batch = array(
    'title' => t('Sleeping'),
    'operations' => array(),
    'finished' => 'batch_example_finished',
    // 'file' => drupal_get_path('module', 'mymodule') . '/mymodule.batch.inc',
  );
  batch_set($batch);

  // Add a batch set with simple operations taking an argument.
  $batch = array(
    'title' => t('Sleep'), // Not displayed.
    'operations' => array(
      array('batch_sleep', array(1)),
      array('batch_sleep', array(2)),
    ),
  );
  batch_set($batch);

  // Add a batch set with an advanced operation taking no argument, but using
  // a private sandbox instead.
  $batch = array(
    'title' => t('Repeated operation'), // Not displayed.
    'operations' => array(
      array('batch_advanced', array()),
    ),
  );
  batch_set($batch);

  // Batch processing has to be invoked manually in D5. The first argument to
  // batch_process() is the path to redirect to after the batch was finished.
  batch_process('');
}

/**
 * Batch operation callback.
 *
 * @param ...
 *   Any arguments passed into operation in batch_set().
 * @param $context
 *   Batch context array, passed by reference:
 *   - 'sandbox': An (initially empty) value to optionally store any values that
 *     persist throughout this operation.
 *   - 'results': An (initially empty) value to store the results of a batch set
 *     that are passed on to the 'finished' callback of the batch set.
 *   - 'finished': Boolean/integer whether the current operation was finished.
 *     Only use this for long operations that recursively need to call
 *     themselves (such as module update functions ran by update.php). Best
 *     practice is to setup a batch set with as many operations you need to run
 *     instead, since recursively calling one operation breaks the progress
 *     indicator.
 *   - 'message': A message to display after the operation was executed.
 */
function batch_sleep($time, &$context) {
  sleep($time * 2);
}

/**
 * Batch operation callback (advanced).
 *
 * @see batch_sleep()
 */
function batch_advanced(&$context) {
  // Note that Batch API runs as many operations that fit into a certain
  // time-frame. If we don't sleep here, you wouldn't *see* any messages or
  // progress of this operation.
  sleep(1);

  // Setup our things to do.
  if (!isset($context['sandbox']['ids'])) {
    $context['sandbox']['ids'] = range(0, 10);
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['max'] = count($context['sandbox']['ids']);
  }

  // Do something.
  $current = array_shift($context['sandbox']['ids']);
  $context['sandbox']['progress']++;
  
  // Return the status of this operation.
  if ($context['sandbox']['progress'] < $context['sandbox']['max']) {
    $context['finished'] = 0;
    $context['message'] = 'Converted ' . $current;
  }
  else {
    $context['finished'] = 1;
  }
}

/**
 * Batch finished handler for example batch.
 */
function batch_example_finished($success, $results, $operations) {
  if ($success) {
    $message = t('Finished @num_nids nodes created successfully.', array('@num_nids' => $results['num_nids']));
  }
  else {
    $message = t('Finished with an error.');
  }
  drupal_set_message($message);
}

